package com.grimpanda.product.content.dto;

import com.grimpanda.product.common.util.ModelMapperProvider;
import com.grimpanda.product.content.entity.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Slf4j
public class ArtworkInfo {

    private static final ModelMapperProvider MODEL_MAPPER_PROVIDER;

    static {
        MODEL_MAPPER_PROVIDER = ModelMapperProvider.getInstance();
    }
    private ArtworkInfo() {
        throw new IllegalStateException("Parent DTO class");
    }


    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Schema(title = "Request for new registration of Artwork")
    public static class RegisterRequest {

        @Schema(description = "title", example = "그림1")
        private String title;

        @Schema(description = "description", example = "그림1 설명")
        private String description;

        @Schema(description = "price", example = "50000")
        private Long price;

        @Schema(description = "contentId", example = "d46f26b663e74dc693d25edf9b7e7050", hidden = true)
        private String contentId;

        @Schema(description = "status", example = "검수중", hidden = true)
        private Status status;

        @Setter
        @Schema(description = "author", example = "jh", hidden = true)
        private String author;

        @Schema(description = "stopYn", example = "N", hidden = true)
        private StopYn stopYn;

        @Schema(description = "file")
        MultipartFile file;

        @Builder
        public RegisterRequest(String title, String description, Long price, String contentId, String author, Status status, StopYn stopYn, MultipartFile file) {
            this.title = title;
            this.description = description;
            this.price = price;
            this.contentId = contentId;
            this.author = author;
            this.status = status;
            this.stopYn = stopYn;
            this.file = file;
        }

        public Artwork toNewEntity(String contentId, String author, Status status, StopYn stopYn) {
            return MODEL_MAPPER_PROVIDER
                    .get()
                    .map(this, Artwork.ArtworkBuilder.class)
                    .contentId(contentId)
                    .author(author)
                    .status(status)
                    .stopYn(stopYn)
                    .createdAt(new Date())
                    .build();
        }

    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Schema(title = "Update for Artwork")
    public static class UpdateRequest {
        private Integer id;
        private Status status;
        private String inspector;
        private DenyType denyType;
        private String comment;
        private StopYn stopYn;
    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Schema(title = "Search for Artwork Info")
    public static class SearchArtwork {
        private Integer id;
        private String title;
        private String description;
        private Long price;
        private String contentId;
        private Status status;
        private String author;
        private String inspector;
        private DenyType denyType;
        private String comment;
        private StopYn stopYn;
        private Date createdAt;
        private String contentUrl;

        @Builder
        public SearchArtwork(Integer id, String title, String description, Long price, String contentId, Status status, String author, String inspector, DenyType denyType, String comment, StopYn stopYn, Date createdAt, String contentUrl) {
            this.id = id;
            this.title = title;
            this.description = description;
            this.price = price;
            this.contentId = contentId;
            this.status = status;
            this.author = author;
            this.inspector = inspector;
            this.denyType = denyType;
            this.comment = comment;
            this.stopYn = stopYn;
            this.createdAt = createdAt;
            this.contentUrl = contentUrl;
        }

        public static ArtworkInfo.SearchArtwork of(Artwork artwork, String url) {
            return MODEL_MAPPER_PROVIDER.get()
                    .map(artwork, SearchArtworkBuilder.class)
                    .contentUrl(url)
                    .build();
        }

    }

}
