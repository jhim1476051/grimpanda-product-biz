package com.grimpanda.product.content.dto;

import com.grimpanda.product.common.util.ModelMapperProvider;
import com.grimpanda.product.content.entity.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Slf4j
public class ProductInfo {

    private static final ModelMapperProvider MODEL_MAPPER_PROVIDER;

    static {
        MODEL_MAPPER_PROVIDER = ModelMapperProvider.getInstance();
    }
    private ProductInfo() {
        throw new IllegalStateException("Parent DTO class");
    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Schema(title = "Request for new registration of Artwork")
    public static class RegisterRequest {

        @Schema(description = "title", example = "그림1")
        private String title;

        @Schema(description = "description", example = "그림1 설명")
        private String description;

        @Schema(description = "price", example = "50000")
        private Long price;

        @Schema(description = "contentId", example = "d46f26b663e74dc693d25edf9b7e7050", hidden = true)
        private String contentId;

        @Setter
        @Schema(description = "author", example = "jh", hidden = true)
        private String seller;

        @Schema(description = "stopYn", example = "N", hidden = true)
        private StopYn stopYn;

        @Schema(description = "file")
        MultipartFile file;

        @Builder
        public RegisterRequest(String title, String description, Long price, String contentId, String seller, StopYn stopYn, MultipartFile file) {
            this.title = title;
            this.description = description;
            this.price = price;
            this.contentId = contentId;
            this.seller = seller;
            this.stopYn = stopYn;
            this.file = file;
        }

        public Product toNewEntity(String contentId, String seller, StopYn stopYn) {
            return MODEL_MAPPER_PROVIDER
                    .get()
                    .map(this, Product.ProductBuilder.class)
                    .contentId(contentId)
                    .seller(seller)
                    .stopYn(stopYn)
                    .createdAt(new Date())
                    .build();
        }

    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Schema(title = "Update for Product")
    public static class UpdateRequest {
        private Integer id;
        private StopYn stopYn;
    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Schema(title = "Search for Product Info")
    public static class SearchProduct {
        private Integer id;
        private String title;
        private String description;
        private Long price;
        private String contentId;
        private String seller;
        private StopYn stopYn;
        private Date createdAt;
        private String contentUrl;

        @Builder
        public SearchProduct(Integer id, String title, String description, Long price, String contentId, String seller, StopYn stopYn, Date createdAt, String contentUrl) {
            this.id = id;
            this.title = title;
            this.description = description;
            this.price = price;
            this.contentId = contentId;
            this.seller = seller;
            this.stopYn = stopYn;
            this.createdAt = createdAt;
            this.contentUrl = contentUrl;
        }

        public static SearchProduct of(Product product, String url) {
            return MODEL_MAPPER_PROVIDER.get()
                    .map(product, SearchProductBuilder.class)
                    .contentUrl(url)
                    .build();
        }
    }

}
