package com.grimpanda.product.content.dto;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import lombok.Builder;
import org.springframework.web.multipart.MultipartFile;

public class MultipartFileImpl implements MultipartFile {

    private String name;
    private String originalFilename;
    private byte[] fileContent;
    private String contentType;

    @Builder
    public MultipartFileImpl(String name, String originalFilename, byte[] fileContent,
                             String contentType) {
        this.name = name;
        this.originalFilename = originalFilename;
        this.fileContent = fileContent;
        this.contentType = contentType;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getOriginalFilename() {
        return originalFilename;
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public boolean isEmpty() {
        return fileContent == null || fileContent.length == 0;
    }

    @Override
    public long getSize() {
        if(isEmpty()) return 0;
        return fileContent.length;
    }

    @Override
    public byte[] getBytes() throws IOException {
        return fileContent;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(fileContent);
    }

    @Override
    public void transferTo(File dest) throws IOException, IllegalStateException {
        try (FileOutputStream fileOutputStream = new FileOutputStream(dest)) {
            fileOutputStream.write(fileContent);
        }
    }
}
