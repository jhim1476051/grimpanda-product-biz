package com.grimpanda.product.content.dto;

import com.grimpanda.product.common.util.ModelMapperProvider;
import com.grimpanda.product.content.entity.Artwork;
import com.grimpanda.product.content.entity.Product;
import com.grimpanda.product.content.entity.Status;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;

import java.util.Date;

@Slf4j
public class ShoppingbagInfo {

    private static final ModelMapperProvider MODEL_MAPPER_PROVIDER;

    static {
        MODEL_MAPPER_PROVIDER = ModelMapperProvider.getInstance();
    }
    private ShoppingbagInfo() {
        throw new IllegalStateException("Parent DTO class");
    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Schema(title = "Request for sending to Shopping bag")
    public static class RegisterRequestFeign {
        @Schema(description = "artworkId", example = "1")
        private Integer artworkId;

        @Schema(description = "goodsId", example = "1")
        private Integer goodsId;

        @Schema(description = "amount", example = "3")
        private Integer amount;
    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Builder
    @AllArgsConstructor
    @Schema(title = "Sending to Shopping bag Detail Info")
    public static class RequestDetail {
        @Schema(description = "artwork shopping bag info")
        private ArtworkInfo artwork;

        @Schema(description = "product shopping bag info")
        private ProductInfo product;

        @Schema(description = "amount")
        private Integer amount;
    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor
    @Builder
    @Schema(title = "artwork Shopping bag Detail Info")
    public static class ArtworkInfo {
        private Integer id;
        private String title;
        private String description;
        private Long price;
        private String contentId;
        private String author;
        private Date createdAt;
    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor
    @Builder
    @Schema(title = "product Shopping bag Detail Info")
    public static class ProductInfo {
        private Integer id;
        private String title;
        private String description;
        private Long price;
        private String seller;
        private String contentId;
        private Date createdAt;

    }



}
