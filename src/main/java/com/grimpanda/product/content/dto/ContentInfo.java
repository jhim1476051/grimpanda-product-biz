package com.grimpanda.product.content.dto;

import com.grimpanda.product.common.util.DateTimeUtil;
import com.grimpanda.product.common.util.ModelMapperProvider;
import com.grimpanda.product.content.entity.Content;
import com.grimpanda.product.content.entity.MemberType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.With;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ContentInfo {
    private static final ModelMapperProvider MODEL_MAPPER_PROVIDER;

    static {
        MODEL_MAPPER_PROVIDER = ModelMapperProvider.getInstance();
    }

    private ContentInfo() {
        throw new IllegalStateException("Parent DTO class");
    }

    @ToString
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @Schema(title = "ContentType")
    public static class Detail {

        @Schema(description = "Content ID", example = "0d8041cda19746c98d14dd67151f65eb")
//        @NotBlank
//        @Size(min = 1, max = 60)
//        @Pattern(regexp = "[0-9a-z]{1,60}")
        private String id;

        @Schema(description = "Full Filename", example = "sea_and_mercury_at_dawn.jpeg")
//        @Size(min = 1, max = 255)
        private String fileName;

        @Schema(description = "MIME type", example = "image/png")
//        @Size(min = 1, max = 255)
        private String contentType;

        @With
        @Schema(description = "Content URL<br/>Users can download at this URL.",
                example = "https://infura-ipfs.io/ipfs/QmeJJHn11yFDh7wRM4njKJBbJL9wcAC1i3aUroavF44Zwj")
//        @Size(min = 1, max = 500)
        private String url;

        @Schema(description = "Content Storage Type")
//        @Size(min = 1, max = 50)
        private String storageType;

        @Schema(description = "File size (byte)")
        private Long fileSize;

        @Schema(description = "(CUSTOMER, SUPPLIER, ADMIN)")
        private MemberType memberType;

        @Builder
        public Detail(String id, String fileName, String contentType, String url, String storageType, Long fileSize, MemberType memberType) {
            this.id = id;
            this.fileName = fileName;
            this.contentType = contentType;
            this.url = url;
            this.storageType = storageType;
            this.fileSize = fileSize;
            this.memberType = memberType != null ? memberType : MemberType.SUPPLIER;
        }

        public static Detail of(Content entity) {
            return MODEL_MAPPER_PROVIDER.get().map(entity, DetailBuilder.class).build();
        }

        public Content toNewEntity() {
            return MODEL_MAPPER_PROVIDER
                    .get()
                    .map(this, Content.ContentBuilder.class)
                    .createdAt(DateTimeUtil.nowDateTime())
                    .build();
        }
    }
}
