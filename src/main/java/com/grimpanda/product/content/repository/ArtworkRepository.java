package com.grimpanda.product.content.repository;

import com.grimpanda.product.content.entity.Artwork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArtworkRepository extends JpaRepository<Artwork, Integer> {

    List<Artwork> findByAuthor(String userId);

    @Query(value = """
            select a from Artwork a
             where 1 = 1
             order by a.createdAt desc
            """)
    List<Artwork> findAllArtwork();
}
