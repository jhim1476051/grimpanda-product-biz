package com.grimpanda.product.content.feign.delivery;

import com.grimpanda.product.content.dto.ShoppingbagInfo;
import com.grimpanda.product.content.feign.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "DeliveryFeignClient", url = "${backend.delivery.url}", configuration = FeignClientConfig.class)
//@FeignClient(name = "DeliveryFeignClient", url = "http://localhost:9000", configuration = FeignClientConfig.class)
public interface DeliveryFeignClient {
    @PostMapping(value = "/api/v1/delivery/tenants/{tenantId}/shopping-bag/save")
    ResponseEntity<String> sendShoppingBag(
            @PathVariable("tenantId") String userId,
            @RequestBody List<ShoppingbagInfo.RequestDetail> requestDetails
    );
}
