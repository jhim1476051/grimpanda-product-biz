package com.grimpanda.product.content.service.impl;

import com.grimpanda.product.common.exception.BizException;
import com.grimpanda.product.common.exception.ErrorCode;
import com.grimpanda.product.content.dto.ShoppingbagInfo;
import com.grimpanda.product.content.entity.Artwork;
import com.grimpanda.product.content.entity.Product;
import com.grimpanda.product.content.feign.delivery.DeliveryFeignClient;
import com.grimpanda.product.content.repository.ArtworkRepository;
import com.grimpanda.product.content.repository.ProductRepository;
import com.grimpanda.product.content.service.ShoppingBagService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShoppingBagServiceImpl implements ShoppingBagService {

    private final ArtworkRepository artworkRepository;

    private final ProductRepository productRepository;

    private final DeliveryFeignClient deliveryFeignClient;

    @Override
    public ResponseEntity sendProductToShoppingbag(String userId, List<ShoppingbagInfo.RegisterRequestFeign> registerRequestFeigns) {

        List<ShoppingbagInfo.RequestDetail> requestDetails = new ArrayList<>();

        for(ShoppingbagInfo.RegisterRequestFeign item : registerRequestFeigns) {
            Artwork artwork = artworkRepository.findById(item.getArtworkId())
                    .orElseThrow();
            System.out.println("artwork present!!!" + artwork.getId());
            Product product = productRepository.findById(item.getGoodsId())
                    .orElseThrow();
            System.out.println("product present!!!" + product.getId());
            requestDetails.add(
                    ShoppingbagInfo.RequestDetail.builder()
                    .artwork(ShoppingbagInfo.ArtworkInfo.builder()
                            .id(artwork.getId())
                            .title(artwork.getTitle())
                            .description(artwork.getDescription())
                            .price(artwork.getPrice())
                            .contentId(artwork.getContentId())
                            .author(artwork.getAuthor())
                            .createdAt(artwork.getCreatedAt())
                            .build())
                    .product(ShoppingbagInfo.ProductInfo.builder()
                            .id(product.getId())
                            .title(product.getTitle())
                            .description(product.getDescription())
                            .price(product.getPrice())
                            .seller(product.getSeller())
                            .contentId(product.getContentId())
                            .createdAt(product.getCreatedAt())
                            .build())
                    .amount(item.getAmount())
                    .build());
            System.out.println("requestDetails add Success!!");
        }
        try {
            System.out.println("request feign client!!");
            deliveryFeignClient.sendShoppingBag(userId, requestDetails);
//            deliveryFeignClient.sendShoppingBag(userId);
            System.out.println("request feign client finished !!");
        } catch (BizException e) {
            e.printStackTrace();
            throw new BizException(ErrorCode.INTERNAL_SERVER_ERROR, "서버 오류로 Shopping Bag으로 전송 실패했습니다.");
        }
        return ResponseEntity.ok(requestDetails);
    }
}
