package com.grimpanda.product.content.service.impl;

import com.grimpanda.product.common.exception.BizException;
import com.grimpanda.product.common.exception.ErrorCode;
import com.grimpanda.product.content.dto.ContentInfo;
import com.grimpanda.product.content.dto.ProductInfo;
import com.grimpanda.product.content.entity.Product;
import com.grimpanda.product.content.entity.StopYn;
import com.grimpanda.product.content.repository.ProductRepository;
import com.grimpanda.product.content.service.ContentService;
import com.grimpanda.product.content.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ContentService contentService;
    private final ProductRepository productRepository;

    @Override
    @Transactional
    public ResponseEntity saveProduct(String userId, ProductInfo.RegisterRequest registerRequest) {
        try {
            registerRequest.setSeller(userId);
            ContentInfo.Detail contentDetail = contentService.uploadContent(userId, registerRequest.getFile());
            Product product = registerRequest.toNewEntity(contentDetail.getId(), userId, StopYn.N);
            productRepository.save(product);
            return ResponseEntity.ok(product);
        } catch (BizException e) {
            e.printStackTrace();
            throw new BizException(ErrorCode.INTERNAL_SERVER_ERROR, "서버 오류로 Product 저장에 실패했습니다.");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductInfo.SearchProduct> searchProductDistinctTenant(String userId) {
        List<ProductInfo.SearchProduct> searchProducts = productRepository.findBySeller(userId).stream()
                .map(product -> ProductInfo.SearchProduct.of(product, product.getContent().getUrl()))
                .collect(Collectors.toList());
        return searchProducts;
    }

    @Override
    public List<ProductInfo.SearchProduct> searchAllProducts(StopYn stopYn) {
        return productRepository.findAll(Sort.by(Sort.Order.desc("createdAt"))).stream()
                .filter(product -> stopYn.equals(product.getStopYn()))
                .map(product -> ProductInfo.SearchProduct.of(product, product.getContent().getUrl()))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ResponseEntity updateProduct(String userId, ProductInfo.UpdateRequest updateRequest) {
        Optional<Product> opProduct = productRepository.findById(updateRequest.getId());
        if (opProduct.isEmpty())
            throw new BizException(ErrorCode.NOT_FOUND, "변경 대상 Product를 찾을 수 없습니다.");

        Product product = opProduct.get();
        product.setStopYn(updateRequest.getStopYn());
        productRepository.save(product);
        return ResponseEntity.ok(product);
    }
}
