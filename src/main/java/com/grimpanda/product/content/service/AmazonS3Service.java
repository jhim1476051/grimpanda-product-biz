package com.grimpanda.product.content.service;

import com.amazonaws.ClientConfiguration;
import com.grimpanda.product.common.exception.BizException;
import com.grimpanda.product.common.exception.ErrorCode;
import com.grimpanda.product.common.exception.KnownException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.*;
import java.io.IOException;
import java.io.InputStream;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Slf4j
public class AmazonS3Service {
    private AmazonS3 s3Client;
    private String bucketName;
    private String region;
    private String accessKeyId;
    private String secretAccessKey;


    public AmazonS3Service(@Value("${aws.s3.bucket}") String bucketName,
                           @Value("${aws.region.static}") String region,
                           @Value("${aws.credentials.access-key-id}") String accessKeyId,
                           @Value("${aws.credentials.secret-access-key}") String secretAccessKey
    ) {
        this.bucketName = bucketName;
        this.region = region;
        this.accessKeyId = accessKeyId;
        this.secretAccessKey = secretAccessKey;
    }

    @PostConstruct
    public void init() {
        setS3Client();
        createBucket();
    }

    public void setS3Client() {
        AWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);

        s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(region)
                .build();
        log.info("s3Client : " + s3Client.toString());
    }

    public void createBucket() {
        try {
            HeadBucketRequest headBucketRequest = new HeadBucketRequest(bucketName);
            s3Client.headBucket(headBucketRequest);
            log.info(
                    "The bucket is already existed and you have permission to access it. Bucket name = {}.",
                    bucketName);
        } catch (SdkClientException ex) {
            try {
                if (!s3Client.doesBucketExistV2(bucketName)) {
                    // Because the CreateBucketRequest object doesn't specify a region, the
                    // bucket is created in the region specified in the client.
                    s3Client.createBucket(new CreateBucketRequest(bucketName));

                    // Verify that the bucket was created by retrieving it and checking its location.
                    String bucketLocation = s3Client.getBucketLocation(new GetBucketLocationRequest(
                            bucketName));
                    log.info("Bucket created. Bucket name = {}.", bucketName);
                    log.info("Bucket location: " + bucketLocation);
                } else {
                    log.error("Bucket name ({}) is already existed. Try another bucket name.",
                            bucketName);
                    throw new BizException(ErrorCode.INTERNAL_SERVER_ERROR,
                            "Failed to create AWS S3 bucket.");
                }
            } catch (SdkClientException e) {
                log.error(e.getMessage(), e);
                throw new BizException(ErrorCode.INTERNAL_SERVER_ERROR, "Failed to create AWS S3 bucket.");
            }
        }
    }

    public String uploadObject(MultipartFile file, String objectKey) {
        try {
            ObjectMetadata metaData = new ObjectMetadata();
            metaData.setContentLength(file.getSize());
            metaData.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION); // SSE-S3 encryption

            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectKey, file.getInputStream(), metaData);

            s3Client.putObject(putObjectRequest);
            log.info("Object created. Object name = {}.", objectKey);
        } catch (SdkClientException | IOException e) {
            log.error("Failed to upload object", e);
            throw new BizException(ErrorCode.INTERNAL_SERVER_ERROR, "Failed to upload object.");
        }

        return s3Client.getUrl(bucketName, objectKey).toString();
    }

    public void deleteObject(String uri, String objectKey) throws KnownException {
        AmazonS3URI s3Uri = new AmazonS3URI(uri);
        try {
            if (objectKey != null) {
                s3Client.deleteObject(s3Uri.getBucket(), objectKey);
                log.info("Object deleted. Object bucket name = {}, Object key = {}.", s3Uri.getBucket(), objectKey);
            } else {
                s3Client.deleteObject(s3Uri.getBucket(), s3Uri.getKey());
                log.info("Object deleted. Object bucket name = {}, Object key = {}.", s3Uri.getBucket(), s3Uri.getKey());
            }
        } catch (Exception e) {
            log.error("Cannot delete cause={}, objectKey={}", e.getMessage(), objectKey);
            throw new KnownException(e);
        }
    }

    public byte[] downloadObject(String uri, String objectKey) {
        AmazonS3URI s3Uri = new AmazonS3URI(uri);
        try {
            S3Object s3Object = s3Client.getObject(s3Uri.getBucket(), objectKey);
            return IOUtils.toByteArray(s3Object.getObjectContent());
        } catch (SdkClientException | IOException e) {
            log.error(e.getMessage());
            throw new BizException(ErrorCode.INTERNAL_SERVER_ERROR, "Failed to read object.");
        }
    }

    public void setPrivateObject(String objectKey) throws KnownException {
        try {
            s3Client.setObjectAcl(bucketName, objectKey, CannedAccessControlList.Private);
            log.info("Object ACL set Private. Object key = {}.", objectKey);
        } catch (Exception e) {
            log.error("Cannot change private cause={}, objectKey={}",
                    e.getMessage(), objectKey);
            throw new KnownException(e);
        }
    }

}
