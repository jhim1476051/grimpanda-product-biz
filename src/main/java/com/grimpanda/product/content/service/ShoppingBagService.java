package com.grimpanda.product.content.service;

import com.grimpanda.product.content.dto.ShoppingbagInfo;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ShoppingBagService {
    ResponseEntity sendProductToShoppingbag(String userId, List<ShoppingbagInfo.RegisterRequestFeign> registerRequestFeigns);
}
