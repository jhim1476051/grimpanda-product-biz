package com.grimpanda.product.content.service.impl;

import com.grimpanda.product.common.exception.BizException;
import com.grimpanda.product.common.exception.ErrorCode;
import com.grimpanda.product.common.exception.KnownException;
import com.grimpanda.product.common.util.DateTimeUtil;
import com.grimpanda.product.content.dto.ContentInfo;
import com.grimpanda.product.content.entity.Content;
import com.grimpanda.product.content.entity.MemberType;
import com.grimpanda.product.content.repository.ContentRepository;
import com.grimpanda.product.content.service.AmazonS3Service;
import com.grimpanda.product.content.service.ContentService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Locale;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
//@AllArgsConstructor
public class ContentServiceImpl implements ContentService {
    private static final String S3_PATH = "content";
    private final AmazonS3Service amazonS3Service;
    @Value("${product.content.max-size}")
    private  Integer contentMaxSize;
    @Value("${product.content.temp-dir}")
    private  String tempDirectoryForDownload;
    private final ContentRepository contentRepository;

//    @Autowired
//    public ContentServiceImpl(ContentRepository contentRepository,
//                              AmazonS3Service amazonS3Service,
//                              @Value("${product.content.max-size}") Integer contentMaxSize,
//                              @Value("${product.content.temp-dir}") String tempDirectoryForDownload) {
//        this.contentRepository = contentRepository;
//        this.amazonS3Service = amazonS3Service;
//        this.contentMaxSize = contentMaxSize;
//        this.tempDirectoryForDownload = tempDirectoryForDownload;
//    }


    @Override
    @Transactional
    public ContentInfo.Detail uploadContent(String userId, MultipartFile file) {

        checkSize(file.getSize());
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        Content content =
                makeContent(userId, file.getContentType(), extension, file.getOriginalFilename(), file.getSize());
        String url;
        try {
            url = uploadObject(file, content, content.getObjectKey());
            log.info("Success to upload file to Private Content Storage Server.");
        } catch (Exception e) {
            log.error(
                    "Failed to upload file. Private Content Server Storage exception. e={}",
                    e.getMessage());
            throw new BizException(ErrorCode.INTERNAL_SERVER_ERROR, "File Upload Fail");
        }

        content.setUrl(url);
        Content storedContent = contentRepository.save(content);
        log.info("Success to upload the content(id={})", storedContent.getId());

        return ContentInfo.Detail.of(storedContent);
    }

    @Override
    @Transactional
    public void deleteContent(String userId, String contentId) {
        contentRepository
                .findById(contentId)
                .ifPresentOrElse(
                        entity -> {
                            if (userId != null && entity.getMemberType().equals(MemberType.ADMIN)) {
                                deleteObject(entity);
                            }
                            else {
                                if (userId != null && !entity.getUserId().equals(userId)) { // 업로드 한 사람이 아닌 경우.
                                    log.error(
                                            "Failed to delete the content(id={}). 접근 권한이 없는 content 입니다.",
                                            contentId);
                                    throw new BizException(ErrorCode.INVALID_PARAMETER, "접근 권한이 없는 content 입니다.");
                                }
                                if (!entity.getStorageType().equals("S3")) { // 저장소가 S3이 아닌 경우.
                                    log.error(
                                            "Failed to delete the content(id={}). Content의 storage type이 S3가 아닙니다.",
                                            contentId);
                                    throw new BizException(ErrorCode.INVALID_PARAMETER, "지울 수 없는 파일 입니다.");
                                }
                                deleteObject(entity);
                            }
                        },
                        () -> log.debug("Content Info ( {} ) is not found.", contentId));
    }

    private void deleteObject(Content entity) {
        try {
            amazonS3Service.deleteObject(entity.getUrl(), entity.getObjectKey());
            contentRepository.deleteById(entity.getId());
            log.info("Success to delete content(id={})", entity.getId());
        } catch (KnownException e) {
            try {
                amazonS3Service.setPrivateObject(entity.getObjectKey());
            } catch (KnownException ex) {
                log.error(
                        "Failed to delete the content(id={}). e={}",
                        entity.getId(),
                        ex.getMessage());
                throw new BizException(ErrorCode.INTERNAL_SERVER_ERROR);
            }
        }
    }

    private String uploadObject(MultipartFile file, Content content, String objectKey){
        return amazonS3Service.uploadObject(file, content.getObjectKey());
    }

    private void checkSize(long fileSize) {
        log.info("The size of Upload Request is {}", fileSize);
        if (fileSize > this.contentMaxSize) {
            log.error(
                    "The file size exceeded. Max size is {} bytes.",
                    this.contentMaxSize);
            throw new BizException(ErrorCode.INVALID_PARAMETER, "The file size exceeded.");
        }
    }

    private Content makeContent(String userId, String contentType, String extension,
                                  String originalFileName, Long fileSize) {
        String contentId = generateContentId();
        String objectKey = S3_PATH + "/" + userId + "/" + contentId;
        if (StringUtils.hasLength(extension)) {
            objectKey += "." + extension;
        }
        return Content.builder()
                .createdAt(DateTimeUtil.nowDateTime())
                .id(contentId)
                .userId(userId)
                .contentType(contentType)
                .extension(extension)
                .fileName(originalFileName)
                .objectKey(objectKey)
                .storageType("S3")
                .fileSize(fileSize)
                .memberType(MemberType.ADMIN)
                .build();
    }

    private String generateContentId() {
        return UUID.randomUUID().toString().toLowerCase(Locale.getDefault()).replace("-", "");
    }
}
