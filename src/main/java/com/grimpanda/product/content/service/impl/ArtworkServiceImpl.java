package com.grimpanda.product.content.service.impl;

import com.grimpanda.product.common.exception.BizException;
import com.grimpanda.product.common.exception.ErrorCode;
import com.grimpanda.product.content.dto.ArtworkInfo;
import com.grimpanda.product.content.dto.ContentInfo;
import com.grimpanda.product.content.entity.Artwork;
import com.grimpanda.product.content.entity.Status;
import com.grimpanda.product.content.entity.StopYn;
import com.grimpanda.product.content.repository.ArtworkRepository;
import com.grimpanda.product.content.service.ArtworkService;
import com.grimpanda.product.content.service.ContentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ArtworkServiceImpl implements ArtworkService {

    private final ContentService contentService;
    private final ArtworkRepository artworkRepository;
    @Override
    @Transactional
    public ResponseEntity saveArtwork(String userId, ArtworkInfo.RegisterRequest registerRequest) {
        try {
            registerRequest.setAuthor(userId);
            ContentInfo.Detail contentDetail = contentService.uploadContent(userId, registerRequest.getFile());
            Artwork artwork = registerRequest.toNewEntity(contentDetail.getId(), userId, Status.검수중, StopYn.Y);
            artworkRepository.save(artwork);
            return ResponseEntity.ok(artwork);
        } catch (BizException e) {
            e.printStackTrace();
            throw new BizException(ErrorCode.INTERNAL_SERVER_ERROR, "서버 오류로 Artwork 저장에 실패했습니다.");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ArtworkInfo.SearchArtwork> searchArtworkDistinctTenant(String userId) {
        List<ArtworkInfo.SearchArtwork> searchArtworks = artworkRepository.findByAuthor(userId).stream()
                .map(artwork -> ArtworkInfo.SearchArtwork.of(artwork, artwork.getContent().getUrl()))
                .collect(Collectors.toList());
        List<Artwork> testList = artworkRepository.findAllArtwork();
        log.info(testList.toString());
        return searchArtworks;
    }

    @Override
    @Transactional
    public ResponseEntity updateArtwork(String userId, ArtworkInfo.UpdateRequest updateRequest) {
        Optional<Artwork> opArtwork = artworkRepository.findById(updateRequest.getId());
        if (opArtwork.isEmpty())
            throw new BizException(ErrorCode.NOT_FOUND, "변경 대상 Artwork을 찾을 수 없습니다.");

        Artwork artwork = opArtwork.get();
        // 게제 불가 판정
        if (updateRequest.getStopYn().equals(StopYn.Y)) {
            artwork.setInspector(updateRequest.getInspector());
            artwork.setDenyType(updateRequest.getDenyType());
            artwork.setComment(updateRequest.getComment());
            artwork.setStopYn(StopYn.Y);
            artwork.setStatus(Status.거절);
        } else { // 게제 가능
            artwork.setInspector(updateRequest.getInspector());
            artwork.setStopYn(StopYn.N);
            artwork.setStatus(Status.승인);
        }
        artworkRepository.save(artwork);
        return ResponseEntity.ok(artwork);
    }

    @Override
    public List<ArtworkInfo.SearchArtwork> searchAllArtworks(StopYn stopYn) {
        return artworkRepository.findAllArtwork().stream()
                .filter(artwork -> stopYn.equals(artwork.getStopYn()))
                .map(artwork -> ArtworkInfo.SearchArtwork.of(artwork, artwork.getContent().getUrl()))
                .collect(Collectors.toList());

    }

}
