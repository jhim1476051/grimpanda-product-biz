package com.grimpanda.product.content.service;

import com.grimpanda.product.content.dto.ProductInfo;
import com.grimpanda.product.content.entity.StopYn;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProductService {
    ResponseEntity saveProduct(String userId, ProductInfo.RegisterRequest registerRequest);

    List<ProductInfo.SearchProduct> searchProductDistinctTenant(String userId);

    List<ProductInfo.SearchProduct> searchAllProducts(StopYn stopYn);

    ResponseEntity updateProduct(String userId, ProductInfo.UpdateRequest updateRequest);
}
