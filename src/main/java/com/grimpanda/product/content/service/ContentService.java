package com.grimpanda.product.content.service;

import com.grimpanda.product.content.dto.ContentInfo;
import com.grimpanda.product.content.entity.Content;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ContentService {

    ContentInfo.Detail uploadContent(String userId, MultipartFile file);

    void deleteContent(String userId, String contentId);
//    ContentInfo.Detail getValidContentInfo(String contentId);
//    String getContentUrlNullable(String contentId);
//
////    ContentInfo.Detail storeTokenInfo(String userId, TokenInfo tokenInfo);
//
//    void checkDownload(String contentUrl);
//
//    ContentInfo.Detail uploadIpfs(String userId, String contentId, String contentUrl);
//
//    ContentInfo.Detail switchS3toIpfs(String contentId);
//
//    List<Content> getUnusedS3Contents(Integer unusedDays);

}
