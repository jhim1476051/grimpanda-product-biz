package com.grimpanda.product.content.service;

import com.grimpanda.product.content.dto.ArtworkInfo;
import com.grimpanda.product.content.entity.StopYn;
import org.springframework.http.ResponseEntity;
import java.util.List;

public interface ArtworkService {
    ResponseEntity saveArtwork(String userId, ArtworkInfo.RegisterRequest registerRequest);

    List<ArtworkInfo.SearchArtwork> searchArtworkDistinctTenant(String userId);

    ResponseEntity updateArtwork(String userId, ArtworkInfo.UpdateRequest updateRequest);

    List<ArtworkInfo.SearchArtwork> searchAllArtworks(StopYn stopYn);
}
