package com.grimpanda.product.content.entity;

public enum MemberType {
    CUSTOMER, SUPPLIER, ADMIN
}
