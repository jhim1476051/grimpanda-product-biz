package com.grimpanda.product.content.entity;

public enum Status {
    작성중, 검수중, 승인, 거절
}