package com.grimpanda.product.content.entity;

public enum StopYn {
    Y, N
}