package com.grimpanda.product.content.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.grimpanda.product.common.util.ModelMapperProvider;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@DynamicUpdate
public class Artwork {

    private static final ModelMapperProvider MODEL_MAPPER_PROVIDER;

    static {
        MODEL_MAPPER_PROVIDER = ModelMapperProvider.getInstance();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String description;
    private Long price;
    private String contentId;
    @Setter
    @Enumerated(EnumType.STRING)
    private Status status;
    private String author;
    @Setter
    private String inspector;
    @Setter
    @Enumerated(EnumType.STRING)
    private DenyType denyType;
    @Setter
    private String comment;
    @Setter
    @Enumerated(EnumType.STRING)
    private StopYn stopYn;
    private Date createdAt;

    @JsonIgnore
    @ManyToOne(targetEntity = Content.class, fetch = FetchType.LAZY)
    @JoinColumn(
            name = "contentId",
            referencedColumnName = "id",
            insertable = false,
            updatable = false)
    private Content content;

}
