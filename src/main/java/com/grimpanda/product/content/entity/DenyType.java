package com.grimpanda.product.content.entity;

public enum DenyType {
    지적재산권침해, 선정성비속어정치성향, 품질미흡, 차별불평등, 기타
}