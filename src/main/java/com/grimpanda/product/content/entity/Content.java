package com.grimpanda.product.content.entity;

import java.util.Date;
//import javax.persistence.*;

import com.grimpanda.product.common.util.ModelMapperProvider;
//import jakarta.persistence.EnumType;
//import jakarta.persistence.Enumerated;
//import lombok.AccessLevel;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//import lombok.ToString;
//import org.hibernate.annotations.DynamicUpdate;
import jakarta.persistence.*;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
public class Content {

    private static final ModelMapperProvider MODEL_MAPPER_PROVIDER;

    static {
        MODEL_MAPPER_PROVIDER = ModelMapperProvider.getInstance();
    }

    @Id
    private String id;
    private String userId;
    private String fileName;
    private String extension;
    private String contentType;
    @Setter
    private String url;
    @Setter
    private String objectKey;
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Setter
    private String storageType;

    private Long fileSize;
    @Enumerated(EnumType.STRING)
    private MemberType memberType;

    public static Content copy(String newId, Content content) {
        return MODEL_MAPPER_PROVIDER.get().map(content, ContentBuilder.class)
                .id(newId)
                .build();
    }

}