package com.grimpanda.product.content.controller;

import com.grimpanda.product.common.constant.Path;
import com.grimpanda.product.content.dto.ContentInfo;
import com.grimpanda.product.content.service.ContentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

//import javax.validation.constraints.NotBlank;
//import javax.validation.constraints.Pattern;
//import javax.validation.constraints.Size;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
@RequestMapping(Path.V1.ProductPath.CONTENT)
@AllArgsConstructor
@Slf4j
public class ContentController {

    private ContentService contentService;
    @Operation(summary = "Tenant Content Upload API",
            tags = {"Tenant Content API"},
            description = "Upload a new content file.")
    @PostMapping(
            value = "",
            consumes = {MULTIPART_FORM_DATA_VALUE},
            produces = {APPLICATION_JSON_VALUE})
    public ContentInfo.Detail uploadContent(
            @PathVariable(value = "tenantId") String userId,
            @RequestPart(value = "file")
            @Parameter(description = "File contents") MultipartFile file) {
        return contentService.uploadContent(userId, file);
    }

    @Operation(summary = "Tenant Content Deletion API",
            tags = {"Tenant Content API"},
            description = "Delete the specified content.")
    @DeleteMapping(
            value = Path.V1.ProductPath.CONTENT_ID,
            produces = {APPLICATION_JSON_VALUE})
    public void deleteContent(
            @PathVariable(value = "tenantId") String userId,
            @PathVariable(value = "contentId")
            @Parameter(description = "Content ID", example = "f53bfa15eecc4045ae4c47e6356e9fdd")
            String contentId) {
        contentService.deleteContent(userId, contentId);
    }

}
