package com.grimpanda.product.content.controller;

import com.grimpanda.product.common.constant.Path;
import com.grimpanda.product.content.dto.ArtworkInfo;
import com.grimpanda.product.content.dto.ProductInfo;
import com.grimpanda.product.content.entity.StopYn;
import com.grimpanda.product.content.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static com.grimpanda.product.common.constant.Path.V1.ProductPath.*;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
//@RequestMapping(GOODS)
@RequestMapping(PRODUCT)
@AllArgsConstructor
@Slf4j
public class ProductController {
    private final ProductService productService;

    @Operation(summary = "Tenant Save Goods API",
            tags = {"Goods API"},
            description = "Tenant Save Goods API")
    @PostMapping(
            value = USER + GOOD + SAVE,
            consumes = {MULTIPART_FORM_DATA_VALUE},
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity saveGoods(
            @PathVariable(value = "tenantId") String userId,
            @Parameter(description = "Goods Info")
            ProductInfo.RegisterRequest registerRequest) {
        return productService.saveProduct(userId, registerRequest);
    }

    @Operation(summary = "Tenant Get Goods API",
            tags = {"Goods API"},
            description = "Tenant Get Goods API")
    @GetMapping(
            value = USER + GOOD,
            produces = {APPLICATION_JSON_VALUE})
    public List<ProductInfo.SearchProduct> searchProducts(
            @PathVariable(value = "tenantId") String userId) {
        return productService.searchProductDistinctTenant(userId);
    }

    @Operation(summary = "Tenant Get All Goods API",
            tags = {"Goods API"},
            description = "Tenant Get All Goods API")
    @GetMapping(
            value = GOOD + ALL,
            produces = {APPLICATION_JSON_VALUE})
    public List<ProductInfo.SearchProduct> searchAllProducts(
            @RequestParam StopYn stopYn
//            @PathVariable(value = "tenantId") String userId
    ) {
        return productService.searchAllProducts(stopYn);
    }

    @Operation(summary = "Grant Goods API",
            tags = {"Goods API"},
            description = "Grant Goods API")
    @PutMapping(
            value = USER + GOOD,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity updateProduct(
            @PathVariable(value = "tenantId") String userId,
            @RequestBody ProductInfo.UpdateRequest updateRequest
    ) {
        return productService.updateProduct(userId, updateRequest);
    }

}
