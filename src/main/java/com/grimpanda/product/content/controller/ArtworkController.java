package com.grimpanda.product.content.controller;

import com.grimpanda.product.content.dto.ArtworkInfo;
import com.grimpanda.product.content.entity.Artwork;
import com.grimpanda.product.content.entity.StopYn;
import com.grimpanda.product.content.service.ArtworkService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static com.grimpanda.product.common.constant.Path.V1.ProductPath.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
//@RequestMapping(ARTWORK)
@RequestMapping(PRODUCT)
@AllArgsConstructor
@Slf4j
public class ArtworkController {

    private final ArtworkService artworkService;

    @Operation(summary = "Tenant Save Artwork API",
            tags = {"Artwork API"},
            description = "Tenant Save Artwork API")
    @PostMapping(
            value = USER + ART + SAVE,
            consumes = {MULTIPART_FORM_DATA_VALUE},
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity saveArtwork(
            @PathVariable(value = "tenantId") String userId,
            @Parameter(description = "Artwork Info")
            ArtworkInfo.RegisterRequest registerRequest) {
        return artworkService.saveArtwork(userId, registerRequest);
    }



    @Operation(summary = "Tenant Get Artwork API",
            tags = {"Artwork API"},
            description = "Tenant Get Artwork API")
    @GetMapping(
            value = USER + ART,
            produces = {APPLICATION_JSON_VALUE})
    public List<ArtworkInfo.SearchArtwork> searchArtworks(
            @PathVariable(value = "tenantId") String userId) {
        return artworkService.searchArtworkDistinctTenant(userId);
    }

    @Operation(summary = "Tenant Get All Artwork API",
            tags = {"Artwork API"},
            description = "Tenant Get All Artwork API")
    @GetMapping(
            value = ART + ALL,
            produces = {APPLICATION_JSON_VALUE})
    public List<ArtworkInfo.SearchArtwork> searchAllArtworks(
            @RequestParam StopYn stopYn
//            @PathVariable(value = "tenantId") String userId
    ) {
        return artworkService.searchAllArtworks(stopYn);
    }

    @Operation(summary = "Inspector Grant Artwork API",
            tags = {"Artwork API"},
            description = "Inspector Grant Artwork API")
    @PutMapping(
            value = "/update" + USER + ART,
            produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity updateArtwork(
            @PathVariable(value = "tenantId") String userId,
            @RequestBody ArtworkInfo.UpdateRequest updateRequest
            ) {
        return artworkService.updateArtwork(userId, updateRequest);
    }



}
