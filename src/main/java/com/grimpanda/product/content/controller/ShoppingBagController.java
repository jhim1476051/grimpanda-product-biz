package com.grimpanda.product.content.controller;

import com.grimpanda.product.common.constant.Path;
import com.grimpanda.product.content.dto.ShoppingbagInfo;
import com.grimpanda.product.content.service.ShoppingBagService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(Path.V1.ProductPath.SHOPPINGBAG)
@AllArgsConstructor
@Slf4j
public class ShoppingBagController {

    private final ShoppingBagService shoppingBagService;

    @Operation(summary = "Tenant Shopping bag API",
            tags = "Shopping bag API",
            description = "Tenant Shopping bag API")
    @PostMapping(
            value = "",
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity sendProductToShoppingbag(
            @PathVariable(value = "tenantId") String userId,
            @RequestBody List<ShoppingbagInfo.RegisterRequestFeign> registerRequestFeigns) {
        return shoppingBagService.sendProductToShoppingbag(userId, registerRequestFeigns);
    }

}
