package com.grimpanda.product;

import lombok.AllArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@EnableFeignClients
@SpringBootApplication
@AllArgsConstructor
@EnableJpaAuditing // JPA Auditing 기능을 활성화합니다.
public class ProductApplication {

	ApplicationContext context;

	@PostConstruct
	public void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Seoul"));
		Environment env = context.getEnvironment();
	}

	public static void main(String[] args) {
		SpringApplication.run(ProductApplication.class, args);
	}

}
