package com.grimpanda.product.common.util;

import com.grimpanda.product.common.exception.BizException;
import com.grimpanda.product.common.exception.ErrorCode;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoLocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/** The type Date time util. */
public class DateTimeUtil {

    private DateTimeUtil() {
        throw new IllegalStateException("Utility class");
    }

    private static final ZoneId ZONE_ID = ZoneId.of("Asia/Seoul");
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /**
     * Now zoned date time.
     *
     * @return the zoned date time
     */
    public static ZonedDateTime now() {
        return ZonedDateTime.now(ZONE_ID);
    }

    /**
     * Now date time date.
     *
     * @return the date
     */
    public static Date nowDateTime() {
        return from(nowLocalDateTime());
    }

    /**
     * Now local date time local date time.
     *
     * @return the local date time
     */
    public static LocalDateTime nowLocalDateTime() {
        return LocalDateTime.now(ZONE_ID);
    }

    /**
     * Today local date.
     *
     * @return the local date
     */
    public static LocalDate today() {
        return LocalDate.now(ZONE_ID);
    }

    /**
     * Gets zoned date time.
     *
     * @param date the date
     * @return the zoned date time
     */
    public static ZonedDateTime getZonedDateTime(final Date date) {
        return ZonedDateTime.ofInstant(date.toInstant(), ZONE_ID);
    }

    /**
     * Gets local date time.
     *
     * @param date the date
     * @return the local date time
     */
    public static LocalDateTime getLocalDateTime(final Date date) {
        if(date == null) return null;
        return Instant.ofEpochMilli(date.getTime()).atZone(ZONE_ID).toLocalDateTime();
    }

    /**
     * Gets local date.
     *
     * @param date the date
     * @return the local date
     */
    public static LocalDate getLocalDate(final Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZONE_ID).toLocalDate();
    }

    /**
     * Parse local date.
     *
     * @param datetime the datetime
     * @param pattern the pattern
     * @return the local date
     */
    public static LocalDate parse(final String datetime, final String pattern) {
        return LocalDate.parse(datetime, DateTimeFormatter.ofPattern(pattern));
    }

    public static Date parse(final String datetime) {
        if (datetime == null || datetime.equals(""))  return null;
        else {
            SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                return transFormat.parse(datetime);
            } catch (Exception e) {
                throw new BizException(ErrorCode.INVALID_PARAMETER, "날짜 형식(" + datetime + ")이 올바르지 않습니다.");
            }
        }
    }

    public static Date parsePost(final String datetime) {
        if (datetime == null || datetime.equals(""))  return null;
        else {
            SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                return transFormat.parse(datetime + " 23:59:59");
            } catch (Exception e) {
                throw new BizException(ErrorCode.INVALID_PARAMETER, "날짜 형식(" + datetime + ")이 올바르지 않습니다.");
            }
        }
    }

    /**
     * Time between long.
     *
     * @param start the start
     * @param end the end
     * @param unit the unit
     * @return the long
     */
    public static long timeBetween(
            final LocalDate start, final LocalDate end, final ChronoUnit unit) {
        return unit.between(start, end);
    }

    /**
     * Time between long.
     *
     * @param start the start
     * @param end the end
     * @param unit the unit
     * @return the long
     */
    public static long timeBetween(
            final ZonedDateTime start, final ZonedDateTime end, final ChronoUnit unit) {
        return unit.between(start, end);
    }

    /**
     * Gets date as string.
     *
     * @param date the date
     * @return the date as string
     */
    public static String getDateAsString(final ChronoLocalDate date) {
        return date.format(DATE_FORMATTER);
    }

    /**
     * From date.
     *
     * @param dateTime the date time
     * @return the date
     */
    public static Date from(final LocalDateTime dateTime) {
        if(dateTime == null) return null;
        return Date.from(dateTime.atZone(ZONE_ID).toInstant());
    }

    /**
     * From date.
     *
     * @param localDate the local date
     * @return the date
     */
    public static Date from(final LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZONE_ID).toInstant());
    }

    /**
     * Gets difference days.
     *
     * @param past the past
     * @param future the future
     * @return the difference days
     */
    public static long getDifferenceDays(Date past, Date future) {
        return TimeUnit.DAYS.convert(future.getTime() - past.getTime(), TimeUnit.MILLISECONDS);
    }

    public static Date plusDays(Date date, int days){
        if(date == null) return null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }
}