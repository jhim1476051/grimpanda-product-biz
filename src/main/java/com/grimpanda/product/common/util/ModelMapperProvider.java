package com.grimpanda.product.common.util;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.internal.util.Strings;
import org.modelmapper.spi.NameTransformer;
import org.modelmapper.spi.NameableType;
import org.modelmapper.spi.NamingConvention;
import org.modelmapper.spi.PropertyType;

public class ModelMapperProvider {
    private final ModelMapper modelMapper;

    private ModelMapperProvider() {
        this.modelMapper = new ModelMapper();
        this.modelMapper
                .getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT)
                .setDestinationNamingConvention(LombokBuilderNamingConvention.instance)
                .setDestinationNameTransformer(LombokBuilderNameTransformer.instance);
    }

    private static class MapperHolder {
        static final ModelMapperProvider MODEL_MAPPER_PROVIDER = new ModelMapperProvider();
    }

    public static ModelMapperProvider getInstance() {
        return MapperHolder.MODEL_MAPPER_PROVIDER;
    }

    public ModelMapper get() {
        return this.modelMapper;
    }

    private static class LombokBuilderNamingConvention implements NamingConvention {

        public static final LombokBuilderNamingConvention instance = new LombokBuilderNamingConvention();

        @Override
        public String toString() {
            return "Lombok @Builder Naming Convention";
        }

        @Override
        public boolean applies(String s, PropertyType propertyType) {
            return PropertyType.METHOD.equals(propertyType);
        }
    }

    private static class LombokBuilderNameTransformer implements NameTransformer {

        public static final NameTransformer instance = new LombokBuilderNameTransformer();

        @Override
        public String transform(final String name, final NameableType nameableType) {
            return Strings.decapitalize(name);
        }

        @Override
        public String toString() {
            return "Lombok @Builder Mutator";
        }
    }
}