package com.grimpanda.product.common.exception;

import java.util.function.Function;
import java.util.function.UnaryOperator;
import org.springframework.http.HttpStatus;

public enum ErrorCode {

    UNAUTHORIZED("UNAUTHORIZED", 40100, str -> "Full authentication is required to access this resource. " + str, HttpStatus.UNAUTHORIZED),

    AUTH_INFO_NOT_FOUND("AUTH_INFO_NOT_FOUND", 40110, str -> "인증 정보를 식별할 수 없습니다.", HttpStatus.UNAUTHORIZED),
    AUTH_INFO_UNAVAILABLE("AUTH_INFO_UNAVAILABLE", 40120, str -> "인증 정보가 유효하지 않습니다.", HttpStatus.UNAUTHORIZED),
    AUTH_INFO_FORBIDDEN("AUTH_INFO_FORBIDDEN", 40310, str -> str + "의 접근 권한이 없습니다.", HttpStatus.FORBIDDEN),
    NOT_FOUND("NOT_FOUND", 40400, str -> str + "이(가) 존재하지 않습니다.", HttpStatus.NOT_FOUND),
    NOT_FOUND_MATCHED("NOT_FOUND_MATCHED", 40401, str -> "요청된 "+str+" 값이 서로 맞지 않습니다.", HttpStatus.NOT_FOUND),
    ALREADY_EXISTS("ALREADY_EXISTS", 40900, str -> str + "이(가) 이미 존재합니다.", HttpStatus.CONFLICT),
    METHOD_NOT_ALLOWED("METHOD_NOT_ALLOWED", 40500, str -> str + " 조건이 맞지 않습니다.", HttpStatus.METHOD_NOT_ALLOWED),
    INVALID_PARAMETER("INVALID_PARAMETER", 40000, str -> "입력값이 올바르지 않습니다. " + str, HttpStatus.BAD_REQUEST),
    INVALID_CONTENT_INFO("INVALID_CONTENT_INFO", 40001, str -> "Content 입력값이 올바르지 않습니다. " + str, HttpStatus.BAD_REQUEST),
    INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR", 50000, str -> "서버 오류 입니다. " + str, HttpStatus.INTERNAL_SERVER_ERROR),
    ACTIVATION_REQUEST_ERROR("ACTIVATION_REQUEST_ERROR", 50033, str -> "계정 Activation 서비스 요청 오류입니다. 입력값을 확인해주십시오.", HttpStatus.BAD_REQUEST),
    ACTIVATION_SERVER_ERROR("ACTIVATION_SERVER_ERROR", 50034, str -> "계정 Activation 서비스 오류입니다.", HttpStatus.INTERNAL_SERVER_ERROR),

    AUTH_SERVER_REQUEST_ERROR("AUTH_SERVER_REQUEST_ERROR", 50035, str -> "인증 서버 요청 오류입니다.", HttpStatus.INTERNAL_SERVER_ERROR),
    AUTH_SERVER_ERROR("AUTH_SERVER_ERROR", 50036, str -> "인증 서버 오류입니다.", HttpStatus.INTERNAL_SERVER_ERROR),

    DATABASE_ERROR("DATABASE_ERROR", 50037, str -> "데이터 베이스 오류가 발생하였습니다." + str, HttpStatus.INTERNAL_SERVER_ERROR),
    DATABASE_QUERY_ERROR("DATABASE_QUERY_ERROR", 50038, str -> "데이터 조회 중 오류가 발생하였습니다.", HttpStatus.INTERNAL_SERVER_ERROR);

    private String codeStr;
    private int code;
    private Function<String, String> message;
    private HttpStatus httpStatus;

    ErrorCode(String codeStr, int code, UnaryOperator<String> message, HttpStatus httpStatus) {
        this.codeStr = codeStr;
        this.code = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public String getCodeStr() {
        return codeStr;
    }

    public int getCode() {
        return code;
    }

    public String getMessage(String str) {
        if (str == null) str = "";
        return message.apply(str);
    }

    public String getMessage() {
        return message.apply("");
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}