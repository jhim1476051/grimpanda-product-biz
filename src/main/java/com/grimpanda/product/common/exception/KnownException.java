package com.grimpanda.product.common.exception;

public class KnownException extends Exception{
    public KnownException(String message){
        super(message);
    }

    public KnownException(Exception e){
        super(e);
    }
}
