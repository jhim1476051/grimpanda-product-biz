# Builder
FROM maven:3.8.3-openjdk-17 as builder
WORKDIR /build

# Maven 파일이 변경되었을 때만 새롭게 의존 패키지 다운로드 받게 함
COPY pom.xml /build/
RUN mvn dependency:go-offline

# 빌더 이미지에서 애플리케이션 빌드
COPY . /build
RUN mvn package

# APP
FROM openjdk:17.0-slim
WORKDIR /app

# 빌더 이미지에서 jar 파일만 복사
COPY --from=builder /build/target/product-0.0.1-SNAPSHOT.jar /app/app.jar

# 환경 변수 설정
ENV AWS_INSTANCE_IP=$AWS_INSTANCE_IP
ENV AWS_INSTANCE_USER=$AWS_INSTANCE_USER
ENV DB_HOST=$DB_HOST
ENV DB_NAME=$DB_NAME
ENV DB_PASSWORD=$DB_PASSWORD
ENV DB_PORT=$DB_PORT
ENV DB_USER=$DB_USER

EXPOSE 8080

# root 대신 nobody 권한으로 실행
USER nobody
ENTRYPOINT ["java", "-jar", "app.jar", "--server.port=8080"]
